const express = require('express');
const app = express();

const data = [{
    id: 1,
    isButtonActive: true,
    BtnName: 'Eng',
    text: 'Hello World'
},
{
    id: 2,
    isButtonActive: false,
    BtnName: 'Fren',
    text: 'Hello some message'
},
{
    id: 3,
    isButtonActive: false,
    BtnName: 'Ger',
    text: 'Hello some other message'
}]

app.get('/pullData', (req, res, next)=>{
    res.send(data);
});

app.listen(3001, ()=>{
    console.log("listening to port 3001")
});


