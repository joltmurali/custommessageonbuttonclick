import React, { useCallback, useEffect, useState } from 'react';
import './App.css';

function App() {

  const [buttonDetails, setButtonDetails] = useState();
  const [message, setMessage] = useState('Hello World Initial States')

  const fetchData = useCallback(() =>{
    fetch('http://localhost:3001/pullData')
    .then((res)=>res.json())
    .then((data)=>setButtonDetails(data))
  }, [setButtonDetails]);

  useEffect(()=>{
    fetchData();
  }, [fetchData]);

  const handleClick = (buttonDetail) =>{
    const newArray = buttonDetails.map((eachItem)=>{
      if(eachItem.id === buttonDetail.id){
        return{
          ...eachItem,
          isButtonActive: true
        }
      } else{
        return{
          ...eachItem,
          isButtonActive: false
        }
      }
    })
    setButtonDetails(newArray);
    setMessage(buttonDetail.text);
  }

  return (
    <div className="container">
      <div className='flex'>
        {buttonDetails && buttonDetails.map((eachItem, index)=>
          <button 
            id={index}
            className={eachItem.isButtonActive ? 'box clicked' : 'box'}
            onClick={()=>handleClick(eachItem)}
          > 
            {eachItem.BtnName}
          </button>
        )}
      </div>
      <div>
        <h2 className='contentFlex'>
          {message}
        </h2>
      
      </div>
    </div>
  );
}

export default App;
